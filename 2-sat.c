/**
 * @author Mehdi MAAREF
 * 
 */
// gcc  -Wall -o gen_2sat 2-sat.c

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <getopt.h>
#include <errno.h>

#ifdef DEBUG
#define debug(args, ...)  printf("[%s]:"args"\n", __FUNCTION__, ##__VA_ARGS__)
#else
#define debug(args, ...) if(verbose) { printf("c "args"\n", ##__VA_ARGS__); }
#endif

#define rand_n (rand()%not_factor ? 1+rand()%n : -(1+rand()%n))

static FILE * f_in = NULL;
static FILE * f_out = NULL;
static int verbose = 0;
static int p = 0;
static int n = 0;
static int not_factor = 2;
static int reverse = 0;
struct timeval tv;

void generate_random_2sat() {
  int i;
  
  if(verbose) {
    fprintf(f_out, "c random generated 2-SAT\n");
    fprintf(f_out, "c not factor: %d\n", not_factor);
  }
  fprintf(f_out, "p cnf %d %d\n", n, p);
  for(i=0; i<p; i++) {
    gettimeofday(&tv, 0);
    srand(tv.tv_usec);
    if(!reverse)
      fprintf(f_out, "%d %d 0\n", rand_n, rand_n);
    else
      fprintf(f_out, "%d %d 0\n", -rand_n, rand_n);
  }
}

void usage(const char* app) {
  fprintf(stdout, "Usage: %s [options] n p\n", app);
  fprintf(stdout, "\t n: number of litterals\n");
  fprintf(stdout, "\t p: number of clauses \n");
  fprintf(stdout, "\t-v: verbose\n");
  fprintf(stdout, "\t-n <not_factor>: factor for random negative litterals\n");
  fprintf(stdout, "\t-i <input_file>\n");
  fprintf(stdout, "\t-o <output_file>\n");
}

int main(int argc, char** argv) {
  int c;
  f_out = stdout;
  f_in = stdin;
  if(argc>=3) {
    gettimeofday(&tv, 0);
    srand(tv.tv_usec);
    while ((c = getopt (argc, argv, "i:o:n:vr")) != -1) {
      switch (c) {
	case 'i':
	  debug("fichier de lecture: %s", optarg);
	  f_in = fopen(optarg, "r");
	  break;
	case 'o':
	  debug("fichier de sortie: %s", optarg);
	  f_out = fopen(optarg, "w");
	  break;
	case 'n':
	  not_factor=atoi(optarg);
	  if(not_factor<2)
	    not_factor=2;
	case 'v':
	  verbose=1;
	  break;
	case 'r':
	  reverse=1;
	  break;
	default:
	  usage(argv[0]);
	  abort();
      }
    }
    n=atoi(argv[argc-2]);
    p=atoi(argv[argc-1]);
  } else {
    usage(argv[0]);
    return 0;
  }
  if(f_in && f_out && n!=0 && p!=0)
    generate_random_2sat();
  else {
    usage(argv[0]);
    return 1;
  }
  fclose(f_in);
  fclose(f_out);
  return 0;
}