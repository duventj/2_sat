#!/usr/bin/env python3
'''
multi_run -i 50 -e 800 -b 100 -c 80 -m 130 -s 150
'''
from cmd import Cmd
import types
import sys, os
import getopt
from datetime import datetime 
from datetime import timedelta
from time import sleep
from gplot import Gnuplot

gen_2sat_cmd = './gen_2sat '
minisat_cmd =  'minisat '


class Py_2Sat(Cmd):
  def __init__(self, **env):
    super().__init__()
    self._env = env
    self.intro = 'Welcome to the 2-SAT workbench'
    self.prompt = '# '
    self.ruler = '-'
    # Commands definition
    self.do_EOF = lambda x: not print('')
    self.help_shell=lambda: print('Run a system command')
    self.gp = Gnuplot(persist=True)
    self.res=0
    self.prob = {}
    self.utime = timedelta()
  def emptyline(self):
    pass
  def do_shell(self, cmd):
    try:
      self.res = os.system(cmd)
    except Exception as e:
      print(e)
  ''' Exit command '''
  def help_exit(self):
    print('Exit the command prompt')
  def do_exit(self, arg):
    return not print('')
  ''' Plot '''
  def do_refresh(self, args=None):
    self.gp.refresh()
  def do_gnuplot_opt(self, args=None):
    if args:
      self.gp.addOption(args)
    else:
      print(self.gp.options)
  def help_gnuplot_opt(self):
    print('Usage: gnuplot_opt [option]')
    print('Send [option] to gnuplot if present')
    print('Read gnuplot options if not')
  def do_plot(self, args, n=0):
    m=len(args.split())
    if not m%2:
      m=int(m/2)
      x = list(map(lambda x: int(x), args.split()[0:m]))
      y = list(map(lambda x: int(x), args.split()[m:2*m]))
      if n == 0:
        self.gp.plot(x, y)
      else:
        self.do_gnuplot_opt("set style line "+ str(n) +" lc rgb '#"+"%6x"%(n)+"' lt 1 lw 2 pt 7 ps 1.5")
        self.gp.plot(x, y,  'with linespoints ls '+str(n))
      self.do_refresh()
    else:
      print('List must be even'+ m)
  def do_gen_2sat(self, line):
    self.do_shell(gen_2sat_cmd+line)
  def do_run(self, line):
    n=100
    c=0.85
    m=1.15
    step=0.02
    it=20
    f=2
    rev=False    
    try:
      opts, args = getopt.getopt(line.split(), 'm:n:c:i:f:s:r')
    except getopt.GetoptError:
      self.help_run()
    for opt, arg in opts:
      if opt in ('-n'):
        n = int(arg)
      elif opt in ('-c'):
        c = int(arg)/100
      elif opt in ('-i'):
        it = int(arg)
      elif opt in ('-m'):
        m = int(arg)/100
      elif opt in ('-f'):
        f = int(arg)
      elif opt in ('-s'):
        step = int(arg)/100
      elif opt in ('-r'):
        rev=True
    try:
      self.run(n, c, m, step, it, f, rev) 
    except KeyboardInterrupt:
      print('Interrupt')
  def help_run(self):
    print('run [-i] [-n] [-c] [-m] [-f] [-r] [-s]')
    print('\t -i nb of iterations for stats')
    print('\t -n nb of litterals')
    print('\t -s step between c')
    print('\t -c starting coeff for clauses')
    print('\t -m maximum coeff')
    print('\t -f negative litterals rate (modulo)')
  def run(self, n=100, c=0.85, m=1.15, step=0.02, it=20, f=2, rev=False):
    gen_args =  ' '
    if(rev):
      gen_args += ' -r '
    self.prob[n] = {}
    t=datetime.now()
    while c<=m:
      cmd = gen_2sat_cmd + gen_args +' '.join([str(n), str(int(c*n))])
      cmd += ' | ' + minisat_cmd
      cmd += ' &>/dev/null'
      #print(cmd)
      cnt=0
      for tmp in range(0, it):
        self.do_shell(cmd)
        if self.res == 2560:
          cnt +=1
        self.res=0          
      self.prob[n][int(c*100)] = int((cnt/it)*100)
      c+=step
    print('n=',n, ', tooks %d usec avg'%((datetime.now()-t)/it).microseconds)
  def do_multi_run(self, args):
    b=100
    e=200
    st=100
    i=25
    cmd = ' '
    try:
      options, args = getopt.getopt(args.split(), 'b:e:m:c:i:f:s:S:')
    except getopt.GetoptError as e:
      print(e)
    for opt, arg in options:
      if opt in ('-c'):
        cmd += ' -c '+ str(arg)
      elif opt in ('-b'):
        b = int(arg)
      elif opt in ('-e'):
        e = int(arg)
      elif opt in ('-i'):
        i = int(arg)
      elif opt in ('-m'):
        cmd += ' -m '+ str(arg)
      elif opt in ('-f'):
        cmd += ' -f '+ str(arg)
      elif opt in ('-s'):
        cmd += ' -f '+ str(arg)
      elif opt in ('-S'):
        st = int(arg)
    cmd += ' -i '+str(i)
    for n in range(b, e+1, st):
      c = cmd + ' -n '+str(n)
      #print(c)
      self.do_run(c)
    self.do_plot_all()
    
  def help_multi_run(self):
    print('Usage: multi_run [-b begin] [-e end] [-s step]')
    print('\t-b b: number of litterals begin')
    print('\t-e nm: number of litterals end')
    print('\t-s step')
    print('Plus the run command options (except n).\nSee the run command for them')
    
  def do_clear(self, args=None):
    self.prob = {}
    self.gp.close()
    self.gp = Gnuplot()
    
  def do_plot_all(self, arg=None):
    self.gp.reset()
    self.gp.title(txt='2-SAT')
    self.gp.xlabel(txt='C')
    self.gp.ylabel(txt='P(sat)')
    self.do_plot('50 150 0 110')
    self.do_gnuplot_opt('set xrange [50:150]') # @todo 100 * c
    self.do_gnuplot_opt('set yrange [0:110]') # ...
    self.do_gnuplot_opt('set multiplot')
    #self.do_gnuplot_opt('set grid')
    #self.do_gnuplot_opt('set key outside')
    for n in self.prob.keys():
      self.do_gnuplot_opt("set style line "+ str(n) +" lc rgb '#"+"%6x"%(n**3%0xffffff)+"' lt 1 lw 1 pt 7 ps 0.3")
    tmp=2
    for n in self.prob.keys():
      x = list(sorted(self.prob[n]))
      y = list(map(lambda v: self.prob[n][v], x))
      self.gp.plot(x, y," title '%s' "%str(n)+" w lp ls "+str(n)) #"u 1:"+str(tmp)+
      tmp+=1
    self.do_gnuplot_opt('unset multiplot')
    
  def do_print_stats(self, arg=None):
    print(self.prob)

  def do_export_svg(self, arg=None):
    self.do_gnuplot_opt('set terminal svg')
    self.do_gnuplot_opt("set output 'out.svg'")
    self.do_plot_all()
    self.do_gnuplot_opt("unset output")
  

if __name__ == "__main__":
  p = Py_2Sat()
  try:
    p.cmdloop()
  except KeyboardInterrupt:
    p.gp.close()
    del p
    print('\nBye Bye...')
